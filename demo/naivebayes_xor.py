# A trivial demo of learning logic operations with naive bayes model.
# Running this couple of times shows variation in the output.
from sklearn.naive_bayes import BernoulliNB
import numpy

N = 150
# Generate N samples of random input with two input booleans:
X = numpy.random.randint(2, size=(N,2))
column1 = X[:,0]
column2 = X[:,1]
# Generate output labels for XOR:
y = column1 + column2 == 1

# Split training and test data:
X_train = X[:.8 * N]
y_train = y[:.8 * N]
X_test = X[.8 * N:]
y_test = y[.8 * N:]

naive_bayes = BernoulliNB()
naive_bayes.fit(X_train, y_train)
print('Naive bayes score for XOR: %f' % naive_bayes.score(X_test, y_test))

# Output labels for AND operation
y = column1 & column2
y_train = y[:.8 * N]
y_test = y[.8 * N:]

naive_bayes.fit(X_train, y_train)
print('Naive bayes score for AND: %f' % naive_bayes.score(X_test, y_test))
