# The purpose here is to demonstrate how PCA tries to identify the most important data in images
from sklearn.svm import SVC
from sklearn import neighbors
from sklearn.naive_bayes import GaussianNB
from sklearn.decomposition.pca import PCA
from sklearn.decomposition import FastICA
import PIL.Image
import numpy
import sys, os
import warnings
import skimage.io

path = "data/CarData/TrainImages/"
images = []
labels = []
for dirpath, dirnames, filenames in os.walk(path):
	for filename in filenames:
		if filename.endswith('.pgm'):
			labels.append(filename.startswith('pos'))
			img = PIL.Image.open(os.path.join(dirpath, filename))
			arr = numpy.array(img.getdata(), numpy.uint16)
			images.append(arr)

X = numpy.array(images)
y = numpy.array(labels)
N = X.shape[0]
r = 0.5
X_train, y_train = X[:r*N], y[:r*N]
X_test, y_test = X[r*N:], y[r*N:]
print "cars in train: {}/{}".format(y_train.sum(), len(y_train))

pca = PCA(n_components=25, whiten=True)
pca.fit(X_train)
print "Percentage of variance explained by top 25 components:",
print ", ".join("{:.4f}".format(i) for i in pca.explained_variance_ratio_)
print "This PCA transformation contains {} of the original information".format(pca.explained_variance_ratio_.sum())
svc = SVC(kernel='rbf')
svc.fit(pca.transform(X_train), y_train)
score = svc.score(pca.transform(X_test), y_test)
print "Score of SVM for the transformed data: {}".format(score)

print "Saving illustration of the first five dimensions of the transformed space."
print "White pixels are considered the most important"
for i in range(5):
	img = pca.components_[i,:].reshape((40,100))
	img = numpy.abs(img)
	scale = 1. / img.max()
	img *= scale
	filename = "car_data_pca_comp{}.png".format(i+1)
	print "  Saving file", filename
	skimage.io.imsave(filename, img)
