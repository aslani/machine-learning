# This demonstrates evaluation of several models for the car detection problem
#
# The data is drawn from the training part of the actual data
# but note that it is still splitted into two separate sets: training and testing.
# There should not be same images in those sets.
#
# The models are evaluated based on their accuracy -- the number of correct 'predictions' on the test set.

from sklearn.svm import SVC
from sklearn import neighbors
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition.pca import PCA
from sklearn.decomposition import FastICA
import PIL.Image
import numpy
import sys, os

import warnings
warnings.filterwarnings("ignore",category=DeprecationWarning)

path = "data/CarData/TrainImages/"
images = []
labels = []
for dirpath, dirnames, filenames in os.walk(path):
	for filename in filenames:
		if filename.endswith('.pgm'):
			labels.append(filename.startswith('pos'))
			img = PIL.Image.open(os.path.join(dirpath, filename))
			arr = numpy.array(img.getdata(), numpy.uint16)
			images.append(arr)

X = numpy.array(images)
y = numpy.array(labels)
N = X.shape[0] # number of images
r = 0.5 # ratio of test/training set division
# Split the data and labels into to sets:
X_train, y_train = X[:r*N], y[:r*N]
X_test, y_test = X[r*N:], y[r*N:]

print "cars in train: {}/{}".format(y_train.sum(), len(y_train))
print

knn = neighbors.KNeighborsClassifier()
knn.fit(X_train, y_train)
print "kNN score:", knn.score(X_test, y_test)
print

nb = GaussianNB()
nb.fit(X_train, y_train)
print "Gaussian naive bayes score:", nb.score(X_test, y_test)
print

print "Support vector machine with two kernels and no preprocessing:"
for kernel in ('linear', 'rbf'):
	scaler = StandardScaler()
	scaler.fit(X_train)
	svc = SVC(kernel=kernel)
	svc.fit(scaler.transform(X_train), y_train)
	score = svc.score(scaler.transform(X_test), y_test)
	print "SVM with {} kernel, score {}".format(kernel, score)
print

print "Support vector machine with two kernels and standard scaling:"
for kernel in ('linear', 'rbf'):
	svc = SVC(kernel=kernel)
	svc.fit(X_train, y_train)
	score = svc.score(X_test, y_test)
	print "SVM with {} kernel, score {}".format(kernel, score)
print

print "Reducing dimensions with principal component analysis (PCA)"
print "The following evaluates models with different number of dimensions produced by PCA"
for components in [5,10,15,20,25,35,50,100,100]:
	pca = PCA(n_components=components, whiten=True)
	pca.fit(X_train)
	for kernel in ('rbf', 'linear'):
		svc = SVC(kernel=kernel)
		svc.fit(pca.transform(X_train), y_train)
		score = svc.score(pca.transform(X_test), y_test)
		print "SVM with {} kernel and {} dimensions, score {}".format(kernel, components, score)
