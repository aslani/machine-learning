# About scikit-learn and something about machine learning #

## Scikit-learn in it's own words is ##

* Simple and efficient tools for data mining and data analysis
* Accessible to everybody, and reusable in various contexts
* Built on NumPy, SciPy, and matplotlib
* Open source, commercially usable - BSD license

## 2.5 Categories of machine learning ##

* [Supervised learning](https://en.wikipedia.org/wiki/Supervised_learning), when you know what your result is supposed to be
* [Unsupervised learning](https://en.wikipedia.org/wiki/Unsupervised_learning), when you don't really know what are you expecting from the data
* [Semi supervised learning](https://en.wikipedia.org/wiki/Semi-supervised_learning), falls inbetween these

## What are we going to do with scikit-learn ##

* Primarily supervised machine learning
* Classification with machine learning

## Classification ##

* We want to predict what category/class sample belongs based on it's features


## Classification with scikit-learn ##

* Estimating or predicting in scikit-learn is done by estimators. 
* Estimators implement fit() and predict() methods
* Fit your training data with your estimator: estimator.fit(features,targets)
* Test your model prediction with new data that was not in your training data: estimator.predict(new_data)

Typical workflow for using estimators in scikit-learn:

0. Pre process your training data into numpy arrays:

* This is the most boring step of it all.
* Try to process your training data into numpy arrays
* You should always keep your features/attributes separate from your targets/labels
* Your target data should have as many targets as you have samples/instances in your features. Vice versa.
* You should only use numeral data

```
#!python

#preprocessing stuff... and finally numpy array
features = np.array([1,2], [2,1], [2,2]])
targets = [1,2,3]

```

1. Initialize estimator object for example:

```
#!python

knn = KNeighborsClassifier(n_neighbors=1)

```

2. Fit your data into model:

```
#!python

knn.fit(features,targets)

```

3. Predict with test data: 

```
#!python

result = knn.predict([1,2])

```

# Perils of overfitting #

Always keep your training data separate from your testing data. Your target is to have very good *generic* fit when testing. Overfit model will also learn noise from your training data when you actually want to learn it's *general* signal.


