import PIL.Image
import numpy
import sys, os

path = "data/CarData/TrainImages/"
images = []
labels = []
for dirpath, dirnames, filenames in os.walk(path):
	for filename in filenames:
		if filename.endswith('.pgm'):
			labels.append(filename.startswith('pos'))
			img = PIL.Image.open(os.path.join(dirpath, filename))
			arr = numpy.array(img.getdata(), numpy.uint16)
			images.append(arr)

X = numpy.array(images)
y = numpy.array(labels)
print('Loaded {} images and corresponding boolean labels indicating presence of a car'.format(X.shape[0]))
print('Each image contains {} pixels'.format(X.shape[1]))
print('Thus, the shape of the data array is: {}'.format(X.shape))
print('The ratio of images containing a car: {}/{}'.format(y.sum(), y.shape[0]))

N = X.shape[0]
r = 0.5
print('Splitting training and test sets with ratio {}'.format(r))
X_train, y_train = X[:r*N], y[:r*N]
X_test, y_test = X[r*N:], y[r*N:]
print "Cars in training set: {}/{}".format(y_train.sum(), len(y_train))
