from sklearn import datasets
from sklearn.neighbors import KNeighborsClassifier
import numpy as np

iris = datasets.load_iris()

features = iris.data
target = iris.target

knn = KNeighborsClassifier(n_neighbors=1)

knn.fit(features, target)

myflower = np.array([1,2,3,6])

result = knn.predict(myflower.reshape(1,-1))
print iris.target_names[result]