from sklearn import datasets
from sklearn.neighbors import KNeighborsClassifier
from skimage import io

digits = datasets.load_digits()

features = digits.data
target = digits.target

knn = KNeighborsClassifier(n_neighbors=1)

knn.fit(features, target)

my_digit_pic = io.imread("../data/8.png", True)

my_digit_array = my_digit_pic.reshape(1,-1)

result = knn.predict(my_digit_array)

print result
