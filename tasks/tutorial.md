# K nearest neighbors with built-in iris dataset #

In this tutorial we'll look  at K nearest neighbors algorithm and apply it to scikit-learn's built-in iris dataset.
Iris dataset contains 150 samples (or instances) of iris flower in three different species.  Data per sample is sepal length, sepal width, petal length and petal width, with species as label.

In traditional table form data would be like this:

| sepal length | sepal width | petal length | petal width | species |
|--------------|-------------|--------------|-------------|---------|
| 5.1          | 3.5         | 1.4          | 0.2         | I.setosa|
| 4.9          | 3.0         | 1.4          | 0.2         | I.setosa|
....
| sepal length | sepal width | petal length | petal width | species |
|--------------|-------------|--------------|-------------|---------|
| 5.9          | 3.0         | 5.1          | 1.8         | I.virginica|

Our task is to predict iris flower species by it's features.

Let's start by importing iris dataset from scikit learn's built-in datasets and K nearest neighbors classifier.

```
#!python

from sklearn import datasets
from sklearn.neighbors import KNeighborsClassifier

iris = datasets.load_iris()
```

iris variable is now of type '<class 'sklearn.datasets.base.Bunch'>'.
If you want, you can also check it's content:
```
#!python

print iris
```

We're mostly interested in two of it's members called *data* and *target*. Iris dataset comes with labels and data in separated numpy arrays so it's nice and easy to work with.
*data* contains our features/attributes: sepal length, sepal width, petal length and petal width.
*target* contains label for that corresponding sample in data array. In this case it would be iris species.

Let's store those two into easy to remember variables

```
#!python

features = iris.data
target = iris.target

#if you want to you also can print these arrays
#print features
#print target

print "Feature data's shape:", features.shape
print "Target data's shape:", target.shape

```

*Note that features array, that will be using for training our model needs to have same amount of indexes as our target array.*


Next we'll need to initialize our classifier. In this tutorial we're using K nearest neighbour classifier. Scikit learn has sensible defaults for their classifiers so it's really easy to use. 

```
#!python

knn = KNeighborsClassifier(n_neighbors=1)

```

n_neighbors -argument here means how many neighbors will our code check when making decision about species of our new unknown flower.

Next let's train our model with our features -data and target labels.

```
#!python

knn.fit(features, target)

```

Finally, let's see if it can predict:

```
#!python

my_flower = [5,3,5,1.8]
result = knn.predict(my_flower)
# as result is numeral, we might want to print something human readable
print iris.target_names[result]

```


