# Detecting if image has a car in it #

Try to implement program that can detect if image has car in it or not. Use [UIUC Image Database for Car Detection](http://cogcomp.cs.illinois.edu/Data/Car/) dataset to train your model. Download dataset, analyse it's structure: find training and testing data. You can use kNN like in our tutorial.

use [io](http://scikit-image.org/docs/dev/api/skimage.io.html) module from skimage to read your image file(s). You get ready numpy array from imread function.

If you like you can use car_data_helper.py file to help you to read image files.

```
#!python

from skimage import io

image_file = io.imread("../data/pic.png")

```

You might need to change your pixel array's dimensions. Use numpy's reshape array function:

```
#!python

#Importing numpy is not mandatory, but sometimes you might want to
#import numpy as np

#this reshapes your array to 1dimensional (horizontal vector)
image_1d = image_file.reshape(1, -1)

back_to_original = image_1d.reshape(8, 8)

```


*bonus: if you want to save images back to graphics use io module's imsave function*

```
#!python

# usage imsave(filename,numpy_array)
io.imsave("../data/some_other_pic.png", back_to_original)

```

extras:

* How would you improve prediction?
* Is there some images in test data that fails even it should not?
* Test your program with a car picture from Internet
* Would [Support Vector Classification](http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html#sklearn.svm.SVC) work better in this task?
