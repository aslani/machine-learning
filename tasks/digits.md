# Detecting hand-written numbers in images #

Try to implement program that can read image file (8x8 pixels, grayscale) and predict what digit is displayed in it.
Import scikit-learns 'digits' -dataset. Analyse it's structure, use KNN classifier to train your model with it.

Try to look at the image data and training data and compare their structure.

use [io](http://scikit-image.org/docs/dev/api/skimage.io.html) module from skimage to read your image file. You get ready numpy array from imread function.

```
#!python

from skimage import io

image_file = io.imread("../data/pic.png")

```

You might need to change your pixel array's dimensions. Use numpy's reshape array function:

```
#!python

#Importing numpy is not mandatory, but sometimes you might want to
#import numpy as np

#this reshapes your array to 1dimensional (horizontal vector)
image_1d = image_file.reshape(1, -1)

back_to_original = image_1d.reshape(8, 8)
```


*bonus: if you want to save images back to graphics use io module's imsave function*

```
#!python

# usage imsave(filename,numpy_array)
io.imsave("../data/some_other_pic.png", back_to_original)

```

extras:

* How would you improve prediction?
* Training data has pixel values from 0-100, but 8 bit grayscale png has pixel values ranging 0-255. Does this matter?
* Do you need more training data if your image resolution is larger or if it has colors?
