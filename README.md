# Supervised machine learning with scikit-learn #

## What ##

Supervised machine learning in python with scikit-learn.

## Setup ##

**Clone helpers and directory structure into your computer**


```
#!shell

git clone https://aslani@bitbucket.org/aslani/machine-learning.git
```


**Install python packages to your system**

* SciPy & NumPy http://www.scipy.org/install.html
* scikit-learn http://scikit-learn.org/stable/install.html
* scikit-image http://scikit-image.org/docs/dev/install.html

[Note from scikit-learn site](http://scikit-learn.org/stable/install.html):
*We don’t recommend installing scipy or numpy using pip on linux, as this will involve a lengthy build-process with many dependencies. Without careful configuration, building numpy yourself can lead to an installation that is much slower than it should be. If you are using Linux, consider using your package manager to install scikit-learn.*

***Please check installation links for information about your system***

In ubuntu scipy & numpy installation would be something like this:

```
#!shell

sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose
```
You can install scikit -packages via pip or conda. I recommend creating virtual environment if you are going to install something via pip.

```
#!shell


virtualenv --system-site-packages ml_venv

source ml_venv/bin/activate

pip install -U scikit-learn
pip install scikit-image
```


or with conda

```
#!shell


conda install scikit-learn
conda install scikit-image
```



**Download data to use in our exercises**

* UIUC Image Database for Car Detection http://cogcomp.cs.illinois.edu/Data/Car/
* GroupLens MovieLens film rating database http://grouplens.org/datasets/movielens/1m/

Extract files into "data" directory

**Test that your installation actually works (in python)**

```
#!python

from sklearn import datasets
from skimage import io
import numpy as np

digits = datasets.load_digits()
```
If you did not get any import errors you seem to be set