## Some nice links to do reading after meetup ##

[Kevin's machine learning tutorial videos](http://blog.kaggle.com/author/kevin-markham/)
[UCI's datasets](http://archive.ics.uci.edu/ml/datasets.html)
[scikit-learn's datasets](http://scikit-learn.org/stable/datasets/)
[scikit-learn's whole documentation](http://scikit-learn.org/stable/documentation.html)